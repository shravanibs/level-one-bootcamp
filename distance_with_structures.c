#include<stdio.h>
#include<math.h>
struct point
{
  float x,y;
};

float dist(struct point a,struct point b)
{
 float d;
 d=sqrt((a.x-b.x)*(a.x-b.x)+(a.y-b.y)*(a.y-b.y));
 return d;
}

int main()
{
 struct point a,b;
 printf("Enter the coordinates of point 1: \n");
 scanf("%f%f",&a.x,&a.y);
 printf("Enter the coordinates of point 2: \n");
 scanf("%f%f",&b.x,&b.y);
 printf("distance between two points is: %f",dist(a,b));
}
