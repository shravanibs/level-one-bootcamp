#include<stdio.h>
#include<math.h>

typedef struct Fractions
{
   int num, den;
}Fractions;

int get_num()
{
   int temp;
   printf("Enter the number of fraction: \n");
   scanf("%d",&temp);
   if(temp < 0)
   {
      printf("Enter a positive number: \n");
      return get_num();
    }
    return temp;
}

void get_input(Fractions *a, int number)
{
   for(int i=0;i<number;i++)
   {
      printf("Enter the numerator:\n");
      scanf("%d",&a[i].num);
      printf("Enter the denominator:\n");
      scanf("%d",&a[i].den);
     }
}

void show_output(Fractions temp, Fractions *a, int number)
{
    printf("The sum of the fractions ");
    for(int i=0;i<number;i++)
    {
       if(i==number-1)
        {
          printf("%d/%d = %d/%d", a[i].num, a[i].den,temp.num,temp.den);
        }
       else
        {
          printf("%d/%d +",a[i].num,a[i].den);
        }
      }
}

int gcd(int x,int y)
{
    if(x==0)
    {
       return y;
    }
    return gcd(y%x,x);
}

Fractions calculate(int number, Fractions *a)
{
   Fractions final;
   int x, y=0, test=1;
   for(int i=0; i<number; i++)
   {
      test = test*a[i].den;
    }
   final.den=test;

for(int i=0;i<number;i++)
  {
   x=a[i].num;
    for(int j=0;j<number;j++)
      { 
         if(i!=j)
        {
           x=x*a[j].den;
        }
    }
    y=y+x;
  }
  final.num=y/gcd(y,test);
  final.den=test/gcd(y,test);
  return final;
}

int main()
{
    int number;
    Fractions final;
    number=get_num();
    Fractions a[number];
    get_input(a,number);
    final=calculate(number,a);
    
    show_output(final,a,number);
    return 0;
}


