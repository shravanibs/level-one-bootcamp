#include<stdio.h>
#include<math.h>

typedef struct frac
{
  int num,den;
}frac;

frac input()
{
  frac f;
  printf("Enter the numerator:\n");
  scanf("%d",&f.num);
  printf("Enter the denominator:\n");
  scanf("%d",&f.den);
  return f;
}

int gcd(int a,int b)
{
  int gcd=1;
  for(int i=2;i<=a&&i<=b;i++)
  {
     if(a%i==0&&b%i==0)
     {
        gcd=i;
     }
    return gcd;
  }
}

frac calc(frac f1,frac f2)
{
   frac f3;
   int  a,b;
   a=(f1.num*f2.den)+(f1.den*f2.num);
   b=f1.den*f2.den;
   int gc=gcd(a,b);
   f3.num=a/gc;
   f3.den=b/gc;
   return f3;
}

void output(frac o)
{
   printf("The sum is: %d/%d",o.num,o.den);
}

int main()
{
  frac q,w,e;
  printf("Enter the 1st fraction\n");
  q=input();
  printf("Enter the 2nd fraction\n");
  w=input();
  e=calc(q,w);
  output(e);
}

